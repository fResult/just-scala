def hi(name: String): Unit = println("Hi: " + name)

object Test extends App {
  hi("Korn")
}
