object TryArguments {
  def main(args: Array[String]): Unit = {
    println(args.length)
    for (arg <- args) {
      println(arg)
    }

    print("Input Your Name: ")
    val name = io.StdIn.readLine()
    print("Input Your Age: ")
    val age = io.StdIn.readInt()
    println(s"Hello $name, you're $age years old.")

    print("Input Radius: ")
    val r = io.StdIn.readInt()
    println(f"${math.Pi * math.pow(r, 2)}")
    println(f"${math.Pi * r * 2}")
  }
}
