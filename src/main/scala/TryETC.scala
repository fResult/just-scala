object TryETC extends App {
  // Apply
  class Car(val year: Int)

  // Companion Object (companion = friend)
  object Car {
    def apply(year: Int) = new Car(year)
  }

  println(Car(2000).year)
  println(Car.apply(2000).year)

  // Collection of Params
  private def display(args: Int*): Unit = args.foreach(println)

  display(30, 50, 70)

  // Algorithm 1
  println("*")
  println("**")
  println("***")
  println("****")

  val s = "*" * 5
  println(s)

  private val starsList = List.tabulate(10)(i => "*" * (i + 1))
  // Algorithm 2
  starsList.foreach(println)

  // Algorithm 3
  for (stars <- starsList) println(stars)

  // Algorithm 4
  for (n <- 1 to 4) {
    println("*" * n)
  }

  // Collection
  // Seq, Set, Map
  // Seq: Array, List
  private val seq1: Seq[Int] = Seq(10, 10, 20)
  seq1.foreach(println)

  private val set1: Set[Int] = Set(10, 10, 20) // Set(10, 20)
  set1.foreach(println)

  private val map1 = Map("One" -> 1, "Two" -> 2)

  private val list1 = List("Apple", "Banana")
  private val list2 = list1.flatMap(_.toUpperCase)
  println(list2)

  private val lst1 = List(1, 2, 3).map(x => "*" * x) // List(*, **, ***)
  private val lst2 = List(1, 2, 3).map("*" * _)
  println(lst1)
  println(lst2)
}
