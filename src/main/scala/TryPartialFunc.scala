import scala.io.Source

object TryPartialFunc extends App {
  private val PartialFunc: Int => Int = {
    case 1 => 10
    case 2 => 20
  }

  println(PartialFunc(1))
//  println(PartialFunc(3))

  private val PartialFunc2 = new PartialFunction[Int, Int] {
    override def isDefinedAt(x: Int): Boolean = x == 1 || x == 2

    override def apply(x: Int): Int = x match {
      case 1 => 10
      case 2 => 20
    }
  }

  private val input = 3
  println(if (PartialFunc2.isDefinedAt(input)) PartialFunc2(input) else 0)

  private val PartialFunc3: PartialFunction[Int, String] = {
    case 1 => "One"
    case 2 => "Two"
    case _ => "Nothing"
  }
  println(PartialFunc3(1))
  println(PartialFunc3(3))

  private def PartialFunc4(x: Int): Int = x match {
    case 1 => 10
    case 2 => 20
    case _ => 0
  }
  println(PartialFunc4(2))
  println(PartialFunc4(3))

  private val chatWithChatBot: PartialFunction[String, String] = {
    case "hi" => "Hi, my name is HAL777."
    case _ => "I don't get it."
  }

  Source.stdin.getLines().foreach(line => println(chatWithChatBot(line)))
//  Source.stdin.getLines().map(chatWithChatBot).foreach(println)
}
