object TryMatching extends App {
  val i = 2
  i match
    case 10 => println("Yeah I'm 10")
    case 2 => println("No, I'm not 10")
    case _ => println("Nothing!")

  val t = ("Korn", 17)
  println(s"Used Tuple: ${t._1} ${t._2}")
  t match {
//    case ("Korn", _) => println("Korn and some number")
    case ("", _) => println("Empty string and some number")
    case (_, 17) => println("Some string and 17")
    case _ => println("Anything else")
  }

  private var forYield = for (i <- 1 to 20) yield {
    (i % 3, i % 5) match {
      case (0, 0) => "fizzbuzz"
      case (0, _) => "buzz"
      case (_, 0) => "fizz"
      case _ => s"$i"
    }
  }
  println(forYield)
}
