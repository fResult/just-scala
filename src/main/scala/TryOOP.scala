object TryOOP extends App {
  private class Car(val year: Int, val model: String) {
    println("start constructor")
    private val machine: String = "motor"

    def run(distance: Int): Unit = {
      println("this car is running around " + distance + " meters" + machine)
    }

    def fill(l: Int): Unit = {
      println("We've filled oil in " + l)
      println("end")
    }
  }

  private val car1 = new Car(2010, model = "BMW")
  println(car1.model + car1.year)
  car1.run(distance = 1000)
  car1.fill(20)

  class Human {
    def eat: Unit = println("Human eats")

    def work: Unit = println("Human works")
  }

  private class ModernHuman extends Human with Doctor with Programmer {
    override def work: Unit = {
      // super.work
      println("Modern Human works smarter than Human")
    }

    override def treat(): Unit = println("Modern Human treats")
    override def writeCode(): Unit = println("Modern Human treats")
  }

  private trait Doctor {
    def treat(): Unit
  }

  private trait Programmer {
    def writeCode(): Unit
  }

  private val mh1 = new ModernHuman
  mh1.work
  mh1.eat
  mh1.treat()
  mh1.writeCode()
}
