import concurrent.Promise
import concurrent.ExecutionContext.Implicits.global
import util.{Failure, Success}
object TryPromise extends App {
  private val promiseInt = Promise[Int]()

  promiseInt.future.onComplete {
    case Success(value) => println(s"Promise Success value: $value")
    case Failure(ex) =>
  }

  new Thread(() => {
    println("Fetching the number...")
    Thread.sleep(200)
    promiseInt.success(99)
    println("Done")
  }).start()
}
