object TrySingleton extends App {
  class Book {
    println("Inside Book")
  }

  // Singleton object
  object Car {
    println("Inside Car")
  }

  Car
  new Book

  object Calculator {
    def add(x: Int, y: Int): Int = x + y

    def add(x: Int, y: Int, z: Int): Int = x + y + z
  }

  private val result1 = Calculator.add(2, 3)
  private val result2 = Calculator.add(2, 3, 4)


  println(s"result1 $result1")
  println(s"result2 $result2")
}
