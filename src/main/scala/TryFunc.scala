object TryFunc extends App {
  private val addFunc1 = (x: Int, y: Int) => x + y

  private val add5 = (x: Int) => addFunc1(5, x)
  println(add5(10))

  private val addFunc2 = new Function[Int, Int] {
    override def apply(x: Int): Int = x + 1
  }
  println(addFunc2(10))

  private val addFunc3 = new((Int, Int) => Int) {
    override def apply(x: Int, y: Int): Int = x + y
  }
  println(addFunc3(5, 7))

  private val joinWithSpace: (String, String) => String = new ((String, String) => String) {
    def apply(x: String, y: String): String = {
      s"$x $y"
    }
  }
  println(joinWithSpace("Hello", "World"))
}
