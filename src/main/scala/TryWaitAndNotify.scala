object TryWaitAndNotify extends App {
  // [?] Container
  // Sample 1 -  Producer -> [?] -> Consumer
  private class SimpleContainer {
    private var value: Int = 0
    def isEmpty: Boolean = value == 0

    def get: Int = {
      val result = value
      value = 0
      result
    }

    def set(newValue: Int): Unit = {
      value = newValue
    }
  }

  // Shared Object/Instance
  private val container = new SimpleContainer

  private val consumer = new Thread(() => {
    println("Consumer is waiting...")
    container.synchronized {
      // Wait until get notify from another thread
      container.wait()
    }
    println(s"Consumer has consumed: ${container.get}")
    println(s"In Consumer, container isEmpty: ${container.isEmpty}") // true
  })

  private val producer = new Thread(() => {
    println("Producer is working...")
    Thread.sleep(1000)
    val x = 100
    container.synchronized {
      container.set(x)
      println(s"In Producer, container isEmpty: ${container.isEmpty}") // false
      println(s"Producer has finished producing")
      container.notify()
    }
  })

  producer.start()
  consumer.start()
}
