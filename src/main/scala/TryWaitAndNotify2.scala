import scala.collection.mutable
import scala.util.Random

object TryWaitAndNotify2 extends App {
  // Sample 2 - Larger container, Producer -> [?, ?, ?] -> Consumer
  private val qContainer = mutable.Queue[Int]()
  private val capacity = 3
  private val random = new Random()

  val consumer = new Thread(() => {
    while (true) {
      qContainer.synchronized {
        if (qContainer.isEmpty) {
          println("[Consumer] Queue is empty, waiting for Producer...")
          qContainer.wait() // Wait until notify
        }
        // Queue is not empty
        val value = qContainer.dequeue()
        println(s"[Consumer] consumed value: $value")
        qContainer.notify()
      }
      Thread.sleep(random.nextInt(500))
    }
  })

  val producer = new Thread(() => {
    var i: Int = 0
    while (true) {
      qContainer.synchronized {
        if (qContainer.size == capacity) {
          println("[Producer] Queue is full, waiting for Consumer...")
          qContainer.wait()
        }
        qContainer.enqueue(i)
        i += 1
        println(s"[Producer] produced $i")
        // Wait until notify
        qContainer.notify()
      }
      Thread.sleep(random.nextInt(500))
    }
  })

  consumer.start()
  producer.start()
}
