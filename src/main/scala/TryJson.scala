import com.google.gson.Gson

object TryJson extends App {
  case class Book(id: Int, name: String)

  private val book1 = Book(1, "Onepiece")
  println(s"Book 1: $book1")
  private val gson = new Gson()
  private val jsonBook = gson.toJson(book1)
  println(s"JSON Book: $jsonBook")

  private val book2 = gson.fromJson(jsonBook, classOf[Book])
  println(s"Book 2: ${book2}")
}
