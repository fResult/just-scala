case object TryCall extends App {
  private def callByValue(n: Long): Unit = {
    println(s"by value: $n")
    println(s"by value: $n")
  }

  private def callByName(n: => Long): Unit = {
    println(s"by name: $n")
    println(s"by name: $n")
  }

  callByValue(System.nanoTime())
  callByName(System.nanoTime())

  private def measure[T](operation: => T): Long = {
    val time = System.currentTimeMillis()
    operation
    System.currentTimeMillis() - time
  }

  private val list = (1 to 1_000_000).toList
  private val usedTime = measure {
    list.map(_ * 1.1)
  }
  println(usedTime)
}
