import language.postfixOps
import concurrent.{Await, Future}
import concurrent.ExecutionContext.Implicits.global
import concurrent.duration.*
import util.{Failure, Success}

object TryFuture extends App {
  private val fResult: Future[Unit] = Future {
    println("Print text in the Future")
  }

  Await.result(fResult, 1 seconds)

  println("Hi")
  println("There")

  private val fResultInt: Future[Int] = Future {
    Thread.sleep(2000)
    100
    // throw new RuntimeException("Let's failed")
  }

  fResultInt.onComplete {
    case Success(value) => println(s"Success: $value")
    case Failure(ex) => ex match {
      case ex: Exception => println(s"Exception: ${ex.getMessage}")
    }
  }
  Thread.sleep(2200)

   private val f1: Future[Int] = Future {
    3
  }
  private val f2: Future[Int] = Future {
    Thread.sleep(1000)
    6
  }
  // for-comprehensive
  private val fSum: Future[Int] = for {
    x <- f1
    y <- f2
  } yield {
    x + y
  }
  fSum.onComplete {
    case Success(value) => println(s"Success::sum: $value")
    case Failure(ex) => ex match {
      case ex: Exception => println(s"Future error: $ex")
    }
  }
  Thread.sleep(1001)
}
