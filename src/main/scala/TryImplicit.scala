import scala.language.implicitConversions

object TryImplicit extends App {
  // Example 1
  case class Person(name: String) {
    def greet: String = s"Hi $name"

  }

  implicit def toPerson(name: String): Person = Person(name)

  println("Korn".greet)

  // Example 2
  def add(x: Int)(y: Int): Int = x + y

  println(add(5)(10))

  private def subtract(x: Int)(implicit y: Int): Int = x - y
  implicit val y: Int = 5
  println(subtract(15))

  // Example 3
  implicit class RichInt(num: Int) {
    def isEven: Boolean = num % 2 == 0
    def sqrt: Double = math.sqrt(num)
  }

  println(s"isEven ${10.isEven}")
  println(s"sqrt ${25.sqrt}")

  // Example 4
  implicit def strToInt(str: String): Int = Integer.valueOf(str)

  println(s"strToInt ${"10" / 2}")
}
