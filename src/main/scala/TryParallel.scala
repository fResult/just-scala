import scala.collection.parallel.ParSeq
object TryParallel extends App {
  private val capacity = 10000
  private val seq = 1 to capacity

  private val identity = (i: Int) => i + 1
  private val parSeq = ParSeq.tabulate(capacity)(identity)

  println(s"Sequence's Size: ${seq.size}")
  println(s"Parallel Sequence's size: ${parSeq.size}")

  private def printWithComma[T]: T => Unit = (value) => print(s"$value, ")

  seq.foreach(printWithComma)
  println
  parSeq.foreach(printWithComma)

  private def measure[T](operation: => T): Long = {
    val time = System.currentTimeMillis
    operation
    System.currentTimeMillis - time
  }

  private val serialTime = measure {
    seq.map(_ + 1)
  }
  private val parallelTime = measure( {
    parSeq.map(_ + 1)
  })

  println
  println(s"Serial time: $serialTime")
  println(s"Parallel time: $parallelTime")
}
