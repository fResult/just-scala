object TryOption extends App {
  private val n1: Option[Int] = Some(1)
  private val n2: Option[Int] = None

  private val foundNum: Option[Int] = Array(6, 5, 4, 3, 2, 1).find(_ > 6)
  println(foundNum.getOrElse(0))

  n1 match {
    case Some(i) => println(s"Found $i")
    case None => println("Not found")
  }

  println(n2.getOrElse(0))

  println(n1.map(_ * 2))
  println(n2.map(_ * 2))

  private val a = Array(10, 20, 30, 40, 50)
  println(a.count(_ % 3 == 0))
  println(a.exists(_ > 40))
  private val arrays = a.partition(i => i >= 40 || i <= 20)
  println(arrays._1.mkString(","))
  println(arrays._2.mkString(","))
}
