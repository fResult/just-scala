object TryLoops extends App {
  for (n <- 1 to 10 if n % 2 == 0) {
    println(n)
  }

  private val yieldedValues = for (n <- 1 until 10 if n % 3 != 0) yield {
    n
  }
  println(yieldedValues)

  private val yieldedPairValues = for {
    n <- 1 to 2
    m <- n to 3
  } yield {
    println(s"$n, $m")
    n * m
  }
  println(yieldedPairValues)

  private def printEachOfYieldedPairs[T](each: T): Unit = println(s"In yielded: $each")

  yieldedPairValues.foreach(
    printEachOfYieldedPairs
  )

  private val mappedYieldedPairs = yieldedPairValues.toList.map(_ * 2)
  println(mappedYieldedPairs)
}