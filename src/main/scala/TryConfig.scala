import com.typesafe.config.ConfigFactory

object TryConfig extends App {
  private val conf = ConfigFactory.load()
  private val what = conf.getInt("what")
  println(what)

  private val bar = conf.getInt("foo.bar")
  println(bar)
  private val name = conf.getString("foo.name")
  println(name)
  private val list = conf.getAnyRefList("foo.list")
  println(list)

  private val foo = conf.getConfig("foo")
  private val bar2 = foo.getInt("bar")
  println(bar2)
}
