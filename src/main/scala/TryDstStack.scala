object TryDstStack extends App {
  private val stack = collection.mutable.Stack[Int](1, 2, 3, 4)
  println(stack)

  stack.push(10, 20)
  println(stack)

  stack.push(30)
  println(stack)
  println(s"Top of stack: ${stack.top}")

  private val poppedNumber = stack.pop()
  println(s"Popped Number: $poppedNumber")
  println(stack)
  println(s"Stack Size: ${stack.size}")

  stack.popWhile(_ != 0)
  println(stack)

  stack.popAll()
  println(stack)

  println(s"Empty?: ${stack.isEmpty}, NotEmpty?: ${stack.nonEmpty}")
  if (stack.nonEmpty) {
    stack.pop()
  }
}
