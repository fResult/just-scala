object TryLazy extends App {
  lazy val x: Int = throw new RuntimeException
  // println(x)

  lazy val y: Int = {
    println("Inside Y")
    10
  }
  println(y) // Evaluate, Inside Y, 10
  println(y) // 10

   private def sideEffect: Boolean = {
    println("Side Effect")
    true
  }
  private def simple: Boolean = false
  private lazy val lazyCondition: Boolean = sideEffect

  println(if (simple && lazyCondition) "Yes" else "No")
}
