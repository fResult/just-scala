object TryString extends App {
  val s = "Hello Korn"
  val c = 'K'

  println(s.take(6))
  println(s.substring(6))
  println(s.substring(6, 8))
  println(s.slice(6, 8))
  println(s.replace("l", "x"))
  println(s.toUpperCase)
  println(s.toLowerCase)
  println(s.length)
  println(s.indexOf("l"))
  println(s.concat("Za"))
  println(s.contains("lo"))
  println(s.toList)
  println(s.toSeq)
  println(s.toSeq.sorted)
  println(s.charAt(4))
  println(s.head)
  println(s.tail.tail)
  println(s.last)
  s.tails.foreach(print)
  s.toArray.foreach(print)
  println
  println(s.patch(5, "Oops", 2))

  private val s1 = "Hello World"
  private val s2 = "01234 56789"
  println(s1.zip(s2))
}
