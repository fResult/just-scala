object TryPolymorphism extends App {
  abstract class Shape {
    def Area: Double
  }

  class Triangle(w: Double, h: Double) extends Shape {
    def Area: Double = w * h * 0.5
  }

  class Square(w: Double, h: Double) extends Shape {
    override def Area: Double = w * h
  }

  private val triangle1 = new Triangle(30, 50)
  println(triangle1.Area)

  private val square1 = new Square(30, 50)
  println(square1.Area)

  private val triangle2: Shape = new Triangle(30, 50)
  println(triangle2.Area)
  private val square2: Shape = new Square(30, 50)
  println(square2.Area)
}
