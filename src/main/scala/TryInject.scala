import com.google.inject.{Binder, Guice, Inject, Module}

trait Processor {
  def process(): Unit
}

class ActualProcessor1 extends Processor {
  override def process(): Unit = println("Actual Processor 1")
}

class ActualProcessor2 extends Processor {
  override def process(): Unit = println("Actual Processor 2")
}

//                           DI. - Dependency Injection, IoC
class OrderService @Inject()(processor: Processor) {
  def processOrder(): Unit = {
    processor.process()
  }
}

class DependencyModule extends Module {
  override def configure(binder: Binder): Unit = {
    // Inject ActualProcessor2
    binder.bind(classOf[Processor]).to(classOf[ActualProcessor2])
  }
}

object TryInject extends App {
  private val injector = Guice.createInjector(new DependencyModule)

  // Can replace new and also can inject
  private val orderService = injector.getInstance(classOf[OrderService])
  orderService.processOrder()

  // New instance for ActualProcessor1
  private val actualProcessor1 = injector.getInstance(classOf[ActualProcessor1])
  actualProcessor1.process()
}
