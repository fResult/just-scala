object TryTupleAndMap extends App {
  // Tuple
  private val tuple1 = (10, "Name 1", "Last Name 1")
  println(s"${tuple1._1}, ${tuple1._2}, ${tuple1._3}")

  private val tuple2 = tuple1.copy(_2 = "Name 2")
  println(s"${tuple2._1}, ${tuple2._2}, ${tuple2._3}")

  // Map
  // Map[Key, Value]
  // - Dictionary
  // - Phonebook
  private val dict = Map.apply[Int, String]((1, "One"), 2 -> "Two")
  println(dict)
  println(dict.keys)
  println(dict.values)
  dict.values.foreach(println)

  private val newPair = (3, "Three")
  private val newDict = dict + newPair
  println(newDict.contains(3))
  println(newDict)
  println(newDict(3))
  println(newDict.toList)

  private val phonebook: Map[String, String] = Map("Somchai" -> "0888888888", "Somying" -> "0999999999")
  println(phonebook)
}
