object TryDstQueue extends App {
  private val queue = collection.mutable.Queue[String]()
  println(queue)

  queue.enqueue("A")
  queue.enqueue("B")
  println(queue)

  queue.enqueueAll(List("C", "D", "E", "F", "G", "H"))

  queue.dequeue()
  println(queue)
  println(s"Queue size: ${queue.size}")

  queue.dequeue()
  println(queue)

  println(s"Empty?: ${queue.isEmpty}, NotEmpty?: ${queue.nonEmpty}")
  while (queue.nonEmpty) {
    queue.dequeue()
    println(queue)
  }
}
