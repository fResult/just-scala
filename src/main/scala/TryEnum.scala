import TryEnum.DayInWeek.DayInWeek

object TryEnum extends App {
  // Approach 1
  object DayInWeek extends Enumeration {
    type DayInWeek = Value
    val Sun, Mon, Tue, Wed, Thu, Fri, Sat = Value
  }

  private def isWorkDay(day: DayInWeek): Boolean = !(day == DayInWeek.Sat || day == DayInWeek.Sun)

  private val day: DayInWeek = DayInWeek.Sun
  if (isWorkDay(day)) {
    println(s"Today is $day. Let's work")
  } else {
    println(s"Today is $day. Let's take a rest")
  }

  // Approach 2
  private object Month {
    val Jan = 1
    val Feb = 2
    val Mar = 3
    val Apr = 4
    val May = 5
    val Jun = 6
    val Jul = 7
    val Aug = 8
    val Sep = 9
    val Oct = 10
    val Nov = 11
    val Dec = 12
  }

  private val month = Month.Jan
  println(Month)
  println(month)
  println(DayInWeek)
  println(Month.Jan)
}