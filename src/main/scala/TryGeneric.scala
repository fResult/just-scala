object TryGeneric extends App {
  // Generic
  class MyList[T](value: T) {
    println("Inside MyList")
    def showType: Unit = {
      if (value.isInstanceOf[Int]) {
        println("This is Int")
      } else if (value.isInstanceOf[String]) {
        println("This is String")
      } else {
        println("Unknown")
      }
    }
  }

  private val intList = new MyList[Int](20)
  private val strList = new MyList[String]("20")
  private val doubleList = new MyList[Double](20.0)

  intList.showType
  strList.showType
  doubleList.showType

  // Companion Object is an Object which has name is the same with the class
  private object MyList {
    def apply[T](value: T): MyList[T] = new MyList[T](value)
    def emptyList[T]: Unit = println("List is cleaned")
  }

  MyList.emptyList[Int]
  MyList.emptyList[String]
  MyList.apply(100).showType
  MyList.apply("Korn").showType

  class Song(name: String, artist: String)
  private object Song {
    def apply(name: String, artist: String): Song = new Song(name, artist)
  }

  // Create New Instance
  Song.apply("My Song", "Me")
}
