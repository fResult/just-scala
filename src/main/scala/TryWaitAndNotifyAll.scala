object TryWaitAndNotifyAll extends App {
  private val bell = new Object
  (1 to 5).foreach(i => {
    new Thread(() => {
      bell.synchronized {
        println(s"Thread $i is waiting...")
        bell.wait() // Wait until notify
      }
      // Print when notified
      println(s"Thread $i is gotten notify!!")
    }).start()
  })

  new Thread(() => {
    // Thread.sleep(2000)
    bell.synchronized {
      bell.notifyAll()
    }
  }).start()
}
