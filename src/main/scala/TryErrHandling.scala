object TryErrHandling extends App {
  private val str: String = "123."
  private val int: Int =  try {
    str.toInt
  } catch {
    case ex: NumberFormatException => {
      println(ex)
      0
    }
    case ex: Exception => {
      println(ex)
      1
    }
  }

  println(s"test $int")
}
