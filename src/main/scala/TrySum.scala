import java.io.PrintWriter
import io.Source

object TrySum extends App {
  private def sum(list: List[Int]): Int = {
    list match {
      case Nil => 0
      case x :: xs => x + sum(xs)
    }
  }

  println(sum(List(10, 20, 30, 40, 50)))

  private val src = Source.fromFile("matrix.txt")
  private val lines = src.getLines()
  private val sumRows = lines.map(line => sum(line.split(" ").map(_.toInt).toList))
//  sumRows.foreach(println)

  private val printWriter = new PrintWriter("output.txt")
  sumRows.toList.foreach(row => {
    printWriter.println(row)
    println(row)
  })
  printWriter.close()

  // Curried Function
  private def add(y: Int)(x: Int): Int = x + y
  private val add3 = add(3)
  val result = add3(5)
  println(s"Added Result: $result")
}

// sum :: List[Int] -> Int
// sum [] = 0
// sum x::xs = x + sum xs
