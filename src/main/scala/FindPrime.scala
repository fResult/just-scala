object FindPrime extends App {
  private def isPrime(n: Int): Boolean = {
    if (n >= 2) {
      var prime = true
      for (i <- 2 until n) {
        if (n % i == 0)
          prime = false
      }
      prime
    } else
      false
  }

  var c = 0

  while (c <= 20) {
    if (isPrime(c)) {
      print(c + " ")
    }
    c += 1
  }

//  println(s"${isPrime(7)}")
}
