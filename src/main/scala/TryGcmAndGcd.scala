object TryGcmAndGcd extends App {

  print("Input Number: ")
  val n = io.StdIn.readInt
  private var arr: Seq[Int] = Seq.empty
  for (i <- 1 to n) {
    print(s"Input #$i: ")
    arr = arr :+ io.StdIn.readInt
  }

  println(s"LCM = ${calculateLCM(arr)}")
  println(s"GCD = ${calculateGCD(arr)}")

  // Input Number: 2
  // Input #1: 4
  // Input #2: 5
  // LCM of 4, and 5 = 20
  private def calculateLCM(xs: Seq[Int]): Int = {
    var s = xs.max
    var isFound = false

    while (!isFound) {
      if (arr.forall(s % _ == 0)) {
        isFound = true
      } else {
        s += 1
      }
    }

    s
  }

  // Input Number: 2
  // Input #1: 4
  // Input #2: 8
  // GCD of 4, and 8 = 4
  private def calculateGCD(xs: Seq[Int]): Int = {
    var s = xs.max
    var isFound = false
    while (!isFound) {
      if (arr.forall(_ % s == 0)) {
        isFound = true
      } else {
        s -= 1
      }
    }
    s
  }
}
