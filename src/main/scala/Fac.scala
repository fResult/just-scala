object Fac extends App {
  private def factorial(n: Int): Int = {
    if (n == 0) {
      1
    } else if (n <= 2) {
      n
    } else {
      n * factorial(n - 1)
    }
  }

  println(s"Fact ${factorial(4)}")
}
