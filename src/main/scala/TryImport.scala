import lecture1.TryFunctions.{add, times}

object TryImport extends App {
  println(add(20, 30))

  println(times(2, 3))
}
