import monix.execution.Scheduler.{global => scheduler}

import java.util.concurrent.TimeUnit

object TryScheduler extends App {
  var i = 0
  private val cancellable = scheduler.scheduleAtFixedRate(
    0, 1, TimeUnit.SECONDS, () => {
      println(s"Hello, world!")
      i+=1
    })

  while (true) {
    Thread.sleep(100)
    if (i >= 4) {
      cancellable.cancel()
    }
  }
}
