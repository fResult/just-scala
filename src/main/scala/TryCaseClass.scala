object TryCaseClass extends App {
  case class Book(id: Int, name: String, author: String)

  // Can be utilize case class without the `new` keyword when create instance
  private val b1 = Book(1, "Onepiece", "Oda")
  private val b2 = Book(2, "Scala Programming", "Martin")

  private def matchCaseClass(book: Book): Unit= {
    book match {
      case Book(1, _, _) => println(s"This is the One Piece book")
      case Book(_, _, "Martin") => println("This is the Scala book")
      case _ => println("Not match any book")
    }
  }

  matchCaseClass(b1)
  matchCaseClass(b2)

  private val b3 = Book(3, "You don't know JS", "Kyle")
  matchCaseClass(b3)

  private case class Product1(id: Int, price: Int)
  println(Product1(1, 100) == Product1(1, 100))
  println(Product1(1, 100) == Product1(2, 100))
  private class Product2(id: Int, price: Int)
  // It will be `false` bec of Product2 is not be a case class
  println(new Product2(1, 100) == new Product2(1, 100))

}
