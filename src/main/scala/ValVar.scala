object ValVar extends App {
  var f: Float = 10f

//  f = f * 2
//  f *= 3
//  println(f.+(3))
//  f += 3
//  if (f == 60) {
//    println("yes")
//  } else {
//    println(s"no $f")
//  }

  private val iifeLiked = {
    val f = 2f
    f * 50
  }

//  println(iifeLiked)
//  private val pi = math.Pi
//  println("%1.2f".format(pi))
//  println(f"$pi%1.3f")

  // BMI = Weight / Height^2
  private def calculateBMI(weight: Int, height: Double): Double = weight / math.pow(height / 100, 2)

  print("Input weight (KG): ")
  private val w = io.StdIn.readInt()
  print("Input height (CM): ")
  private val h = io.StdIn.readInt()
  private val bmi: Double = calculateBMI(w, h)

  if (bmi > 40)
    println(s"Obese Class III - bmi: $bmi")
  else if (bmi >= 35)
    println(s"Obese Class II - bmi: $bmi")
  else if (bmi >= 30)
    println(s"Obese Class I - bmi: $bmi")
  else if (bmi >= 25)
    println(s"Overweight - bmi: $bmi")
  else if (bmi >= 18.5)
    println(s"Normal - bmi: $bmi")
  else if (bmi >= 17)
    println(s"Mild Thinness - bmi: $bmi")
  else if (bmi >= 16)
    println(s"Moderate Thinness - bmi: $bmi")
  else
    println(s"Severe Thinness - bmi: $bmi")

}
