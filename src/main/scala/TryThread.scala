import java.util.concurrent.{ExecutorService, Executors, RejectedExecutionException}

object TryThread extends App {
  //  private val runnable = new Runnable {
  //    override def run(): Unit = {
  //      println(s"In parallel ${Thread.currentThread().getName}")
  //    }
  //  }
  //
  //  private val thread0 = new Thread(runnable)
  //  private val thread1 = new Thread(runnable)
  ////  thread1.setName("MY_THREAD_1")
  //  thread0.start()
  //  thread1.start()
  //
  //  // Wait until thread0 finish working
  //  thread0.join()
  //  println("After Thread0 finish it's working")
  //
  //  private val thread2 = new Thread(() => {
  //    println("Inside Thread 2")
  //  })
  //  thread2.start()

  // Thread Pool
  // - Each pool can have multi-thread
  private val threadPool: ExecutorService = Executors.newFixedThreadPool(3)
//  threadPool.execute(() => {
//    Thread.sleep(1000)
//    println("Inside Thread Pool 1")
//  })
//
//  threadPool.execute(() => {
//    println("Inside Thread Pool 2")
//  })
//
//  threadPool.execute(() => {
//    println("Inside Thread Pool 3")
//  })
//
//  threadPool.shutdown()
//  println(s"Shutdown?: ${threadPool.isShutdown}")
//
//  if (!threadPool.isShutdown) {
//    threadPool.execute(() => {
//      println("Inside Thread Pool 4")
//    })
//  }
//
//  try {
//    threadPool.execute(() => {
//      println("Inside Thread Pool 5")
//    })
//  } catch {
//    case ex: ExceptionInInitializerError => {
//      println(s"OK Initializer Error: $ex")
//    }
//    case ex: RejectedExecutionException => {
//      println(s"OK Rejected Execution: $ex")
//    }
//    case ex: Exception => {
//      println(s"OK Exception: $ex")
//    }
//  }

  class BankAccount(var amount: Int) {
    override def toString: String = "" + amount
  }

  val bankAccount = new BankAccount(5000)
  val t1 = new Thread(() => { bankAccount.amount -= 200 })
  val t2 = new Thread(() => { bankAccount.amount -= 100 })
  t1.start()
  t2.start()
  t1.join(100)
  t2.join(100)
  println(s"Bank Account: $bankAccount")

//  println("\n===================")
//  println("=== End Program ===")
//  println("===================")
}
