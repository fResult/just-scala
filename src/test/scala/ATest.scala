import org.scalatest.funsuite.AnyFunSuite

class ATest extends AnyFunSuite {
  test("Test Calculator double input 10 should return 20") {
    val expected: Int = 20
    val result: Int = Calculator.double(10)
    assert(result === expected)
  }

  test("Test Calculator double input 5 should return 10") {
    val expected: Int = 10
    val result: Int = Calculator.double(5)
    assert(result === expected)
  }
}
