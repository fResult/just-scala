ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.2.2"

lazy val root = (project in file("."))
  .settings(
    name := "JustScala"
  )

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.15" % "test"
libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4"
libraryDependencies += "io.monix" %% "monix" % "3.4.1"
libraryDependencies += "com.typesafe" % "config" % "1.4.2"
libraryDependencies += "com.google.inject" % "guice" % "7.0.0"
libraryDependencies += "com.google.code.gson" % "gson" % "2.10.1"
